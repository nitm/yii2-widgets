<?php

namespace nitm\widgets\controllers;

use Yii;
use nitm\widgets\models\TagCategory;
use nitm\widgets\models\Tag;
use nitm\widgets\models\search\Tag as TagSearch;
use nitm\helpers\Response;
use nitm\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InstructionController implements the CRUD actions for Tag model.
 */
class TagController extends BaseListController
{
	public function init()
	{
		parent::init();
		$this->model = new Tag(['scenario' => 'default']);
	}

	public function behaviors()
	{
		$behaviors = [
		];
		return array_merge_recursive(parent::behaviors(), $behaviors);
	}

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex($type=null, $id=null, $options=[])
    {
		$className = "\\nitm\widgets\\models\\".\nitm\helpers\ClassHelper::properModelName($type);
		return parent::actionIndex($type, $id, [
			'widgetClass' => \nitm\widgets\widgets\Tag::className(),
			'model' => $className::findOne($id),
			'listModel' => new Tag([
				'location_id' => $id
			])
		]);
	}

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($type=null, $id=null, $item=null, $modelClass=null)
    {
		return parent::actionUpdate($type, $id, $item, Tag::className());
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionView($type=null, $id=null, $item=null, $modelClass=null)
    {
		return parent::actionView($type, $id, $item, Tag::className());
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($type=null, $id=null, $item=null, $modelClass=null)
    {
		return parent::actionDelete($type, $id, $item, Tag::className());
    }
}
