<?php

namespace nitm\widgets\controllers;

use nitm\helpers\Response;

class WidgetController extends DefaultApiController
{


	public function behaviors()
	{
		$behaviors = [
			'access' => [
				'only' => ['index'],
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => \yii\filters\VerbFilter::className(),
				'actions' => [
					'index' => ['get', 'post'],
				],
			],
		];

		return array_replace_recursive(parent::behaviors(), $behaviors);
	}

    /**
     * Get widget.
     * @return string widget HTML
     */

    public function actionIndex($type=null, $args=null)
    {
		if(!$this->isResponseFormatSpecified)
			$this->setResponseFormat('html');
		list($widgetClass, $options) = $this->getWidgetOptions($type, \Yii::$app->request->get('options'));

		if(!$widgetClass)
			throw new \yii\web\NotFoundHttpException("Couldn't find $type widget");

		Response::viewOptions('args', [
			'options' => $options,
			'widgetClass' => $widgetClass
		]);
		return $this->renderResponse(null, Response::viewOptions(), \Yii::$app->request->isAjax);
    }

	protected function getWidget($class, $options)
	{
		return $class::widget($options);
	}
	
	protected function getWidgetOptions($class, $options=[])
	{
		$ret_val = '';
		$widgetClass = '';
		$class = \nitm\helpers\ClassHelper::properFormName($class);
		switch(1)
		{
			case class_exists("\nitm\widgets\widgets\\".$class):
			$widgetClass = "\nitm\widgets\widgets\\".$class;
			break;

			default:
			return [];
			break;
		}
		switch(class_exists($widgetClass))
		{
			case true:
			switch(isset($options['model']['type']))
			{
				case true:
				$widgetOptions = [];
				switch(isset($options['model']))
				{
					case true:
					$modelOptions = $options['model'];
					$targetClass = "\\nitm\widgets\models\\".$this->properName($options['model']['type']);
					$widgetOptions['model'] = $targetClass::findOne($modelOptions['id']);
					unset($options['model']);
					break;

					default:
					$modelOptions = [];
					break;
				}
				break;
			}
			$ret_val = array_merge($options, $widgetOptions);
			break;
		}
		return [$widgetClass, $ret_val];
	}
}
