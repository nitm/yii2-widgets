<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 * @version 1.0.0
 */

namespace nitm\widgets\widgets;

use Yii;
use nitm\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use kartik\builder\Form;
use kartik\widgets\ActiveForm;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Icon;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@nitm\widgets.com>
 * @since 1.0
 */
class WikipediaExtractor extends \yii\base\widget
{
	public $form;
	public $options;
	public $model;
	public $wikiModel;
	public $attribute;
	public $descriptionAttribute = 'content';
	public $static = false;

	public function init()
	{
		if(!isset($this->wikiModel))
			$this->wikiModel = new \nitm\widgets\helpers\mediawiki\models\Data([
				'model' => $this->model
			]);
		$this->options['id'] = 'wikipedia-'.$this->model->getId().uniqid();
	}

	public function run()
	{
		ob_start();
		$input = Html::tag('span', 'Search For', [
			'class' => 'input-group-addon'
		])
		.Html::textInput($this->model->formName().'['.$this->attribute.']', $this->model->title(), [
			'placeholder' => 'Enter a string here to search Wikipedia',
			'class' => 'form-control',
			'role' => 'userWikipediaSearch',
		])
		.Html::tag('span', Html::button(Html::tag('span', 'Search ', [
					'class' => 'hide-sm'
			]).Icon::show('wikipedia-w'), [
			'class' => 'btn btn-info',
			'role' => 'getWikipediaInfo',
			'data-parent' => '#'.$this->options['id'].'-div',
			'data-container' => '#'.$this->options['id']
		]), [
			'class' => 'input-group-btn'
		]);

		echo Html::tag('div', $input, [
			'class' => 'input-group'
		])."<br>";

		echo Form::widget([
			'options' => $this->options,
			'model' => $this->wikiModel,
			'staticOnly' => $this->static,
			'form' => $this->form,
			'columns' => 1,
			'attributes' => [
				'title' => [
					'type' => Form::INPUT_TEXT,
					'options' => [
						'role' => 'wikipediaSuggestedTitle',
					],
					'fieldConfig' => [
						'addon' => [
							'prepend' => [
								'asButton' => true,
								'content' => Html::button('Use', [
									'role' => 'setValueFromWikipedia',
									'class' => 'btn btn-primary',
									'data-to' => "[role~='wikipediaTitleInput']",
									'data-from' => "[role~='wikipediaSuggestedTitle']"
								])
							]
						],
					]
				],
				'category' => [
					'type' => Form::INPUT_STATIC,
					'staticValue' => null,
					'label' => 'Categories',
					'options' => [
						'role' => 'wikipediaSuggestedCategory'
					]
				],
				'text' => [
					'type' => Form::INPUT_TEXTAREA,
					'placeholder' => 'Enter a description',
					'fieldConfig' => [
						'addon' => [
							'prepend' => [
								'asButton' => true,
								'content' => Html::button('Use', [
									'role' => 'setValueFromWikipedia',
									'class' => 'btn btn-primary',
									'data-parent' => $this->form->options['id'],
									'data-to' => "[role~='wikipediaDescriptionInput']",
									'data-from' => "[role~='wikipediaSuggestedText']",
								])
							]
						],
					],
					'options' => [
						'role' => 'wikipediaSuggestedText',
					]
				]
			]
		]);
		$contents = ob_get_contents();
		ob_end_clean();
		return Html::tag('div', $contents, [
			'id' => $this->options['id'].'-div',
			'class' => 'alert alert-primary'
		]);
	}
}
