<?php
/**
 * @link http://www.nitm\widgets.com/
 * @copyright Copyright (c) 2014 Wukdo
 */

namespace nitm\widgets\widgets\assets;

use yii\web\AssetBundle;

/**
 * @author Malcolm Paul <lefteyecc@nitm.com>
 */
class TagListAsset extends AssetBundle
{
	public $sourcePath = "@nitm/widgets/widgets/assets/";
	public $css = [
	];
	public $js = [
		'js/base-sortable.js',
		'js/base-list.js',
		'js/tag.js',
	];
	public $depends = [
		'nitm\widgets\assets\AppAsset',
	];
}
