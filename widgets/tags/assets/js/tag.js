'use strict';

class WukdoTagList extends BaseSortable
{
	constructor() {
		super('nitm\widgets-admin:tag');

		this.views = {
			containerId: "[role~='tags']",
			listFormContainerId: "[role~='tagFormContainer']",
			itemId: "tag"
		};
	}
}

$nitm.onModuleLoad('nitm\widgets-admin', function (module) {
	module.initModule(new WukdoTagList());
});
