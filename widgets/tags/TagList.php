<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 * @version 1.0.0
 */

namespace nitm\widgets\widgets;

use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use nitm\widgets\models\search\Tag;
use nitm\widgets\models\TagList as ListModel;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@nitm\widgets.com>
 * @since 1.0
 */
class TagList extends BaseList
{
	public $withForm = true;

	protected $queryDataProvider = 'getTags';
	protected $listClass = 'nitm\widgets\models\Tag';
	protected $listView = '@nitm/widgets/views/tag/index';
	protected $assetClass = '\nitm\widgets\widgets\assets\TagListAsset';
	protected $formWidgetClass = '\nitm\widgets\widgets\TagListForm';
}
