<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 * @version 1.0.0
 */

namespace nitm\widgets\widgets;

use Yii;
use nitm\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use nitm\widgets\models\Image;
use nitm\widgets\models\Tag as ListModel;
use nitm\widgets\helpers\Storage;
use nitm\widgets\widgets\assets\TagListAsset;
use nitm\helpers\Helper;
use nitm\helpers\Response;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@nitm\widgets.com>
 * @since 1.0
 */
class TagListForm extends BaseListForm
{
	public $listItemType = 'tag';

	protected $listClass = '\nitm\widgets\models\Tag';
	protected $formView = '/tag/_form';
}
