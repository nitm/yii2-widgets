<?php

use nitm\helpers\Html;
use yii\widgets\ListView;
use nitm\widgets\models\AmbianceList;
use nitm\helpers\Icon;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var nitm\widgets\models\search\AmbianceList $searchModel
 */

$dataProvider->pagination = false;
$primaryModel = isset($primaryModel) ? $primaryModel : $model->getAmbiances();
?>
<div id="tag-container">
    <?php
		if(@$asList)
			echo ListView::widget([
				'dataProvider' => $dataProvider,
				'itemView' => function ($model, $key, $index, $widget) use($primaryModel) {
					$widget->itemOptions['id'] = 'tag'.$model->getId();
					return $this->render('/tag/view', [
						'model' => $model,
						'primaryModel' => $primaryModel,
						'notAsListItem' => true,
					]);
				},
				'itemOptions' => [
					'tag' => false,
					'class' => 'list-group-item'
				],
				'summary' => false,
				'options' =>[
					'role' => "tags",
					'id' => "tag-list",
					'tag' => 'ul',
					'class' => 'media-list'
				]
			]);
		else
			echo Html::tag('span', \nitm\widgets\metadata\MetaInfo::widget([
				'displayAs' => 'tags',
				'valuesOnly' => true,
				'attributes' => ['value'],
				'items' => $dataProvider->getModels(),
                'options' => [
                    'tag' => false,
                ],
				'itemOptions' => function ($model) {
					return [
                        'tag' => 'span',
						'after' => Html::tag('a', Icon::show('remove'), [
							'href' => '/tag/delete/'.$model->remote_type.'/'.$model->remote_id.'/'.$model->priority,
							'data-method' => 'post',
							'role' => 'deleteAction metaAction',
							'data-parent' => '#tag'.$model->getId(),
						])
					];
				}
			]), [
                'role' => 'tags'
            ]); ?>
</div>
<?php if(\Yii::$app->request->isAjax): ?>
<script type='text/javascript'>
$nitm.onModuleLoad('nitm\widgets-admin:tag', function () {
	$nitm.module('nitm\widgets-admin').initMetaActions('#tag-form-container');
});
</script>
<?php endif; ?>
