<?php

use nitm\helpers\Html;
use kartik\grid\GridView;
use nitm\helpers\Icon;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var nitm\widgets\models\search\AmbianceList $searchModel
 */

$this->title = Yii::t('app', '{listType}s for {type}: {title}', [
    'listType' => $model->properName(),
    'type' => $model->remote->properName(),
    'title' => $model->remote->title()
]);
$this->params['breadcrumbs'][] = $this->title;

$options = array_merge([
    'id' => 'tag-index',
    'role' => 'tagFormContainer'
], (isset($options) ? $options : []));
?>
<?= Html::beginTag('div', $options); ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
        if(isset($withForm) && $withForm !== false)
            echo $widgetFormClass::widget([
    			'model' => $primaryModel
    		]);
    ?>

    <?= $this->render('list', [
        'model' => $model,
        'primaryModel' => $primaryModel,
        'dataProvider' => $dataProvider,
    ]); ?>

</div>
