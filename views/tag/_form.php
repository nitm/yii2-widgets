<?php

use nitm\helpers\Html;
use yii\helpers\Inflector;
use kartik\icons\Icon;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use nitm\widgets\models\Measurement;
use nitm\widgets\models\Ambiance;
use nitm\widgets\models\AmbianceList;
use nitm\widgets\ajax\Dropdown;

/**
 * @var yii\web\View $this
 * @var nitm\widgets\models\Ambiance $model
 * @var yii\widgets\ActiveForm $form
 */

$action = $model->getIsNewRecord() ? 'create' : 'update';
?>

<div class="tag-form">
<?php
    $form = ActiveForm::begin([
		"action" => "/tag/$action?_format=json",
		"type" => ActiveForm::TYPE_INLINE,
		'options' => [
			"role" => "createListItem",
            'id' => 'tag-form'.$model->getId()
		],
		'formConfig' => [
			'showLabels' => false
		],
		'enableAjaxValidation' => true
	]);

	echo Html::tag('h4', "Add Tags");
	echo $form->field($model, 'value');
    echo Html::submitButton(Icon::show('plus').Yii::t('app', ' Create'), [
        'class' => 'form-control hidden-md hidden-sm hidden-xs '.($model->isNewRecord ? 'btn btn-success' : 'btn btn-info'),
    ]);
	echo $form->field($model, 'remote_id')->hiddenInput([
			'value' => $primaryModel->getId()
			])->label("Remote ID", ['class' => 'sr-only']);
	echo $form->field($model, 'priority')->hiddenInput()->label("Priority", ['class' => 'sr-only']);
	echo $form->field($model, 'remote_type')->hiddenInput([
			'value' => $primaryModel->isWhat()
			])->label("Remote Type", ['class' => 'sr-only']);

	ActiveForm::end();
?>

</div>
