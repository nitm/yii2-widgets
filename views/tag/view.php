<?php

use nitm\helpers\Html;

use kartik\grid\GridView;
use nitm\helpers\Icon;
use nitm\widgets\helpers\FormHelper;

/**
 * @var yii\web\View $this
 * @var nitm\widgets\models\AmbianceItems $model
 * @var yii\widgets\ActiveForm $form
 */
$itemId = 'tag'.$model->getId();
?>
<?php if(@$asListItem): ?>
	<?= Html::beginTag('li', [
			'class' => "media tag-item ".\nitm\helpers\Statuses::getListIndicator($model->getStatus()),
			"data-url" => "/tag/set-priority/".$model->id."?_format=json",
			'id' => $itemId,
			'role' => 'statusIndicator'.$model->id
		]);
	?>
	<div class="media-left">
		<a href="#">
			<span class="media-object">
				<?= $model->priority ?>
			</span>
		</a>
	</div>
	<div class="media-body">
		<div class="row">
			<div class="col-sm-12 col-lg-10 col-md-10">
				<h4 class="media-heading"><?= $model->value; ?></h4>
			</div>
		</div>
	</div>
	<div class="media-right">
		<?=
			Html::tag('a', Icon::show('remove'), [
				'href' => '/tag/delete/'.$model->remote_type.'/'.$model->remote_id.'/'.$model->priority,
				'data-method' => 'post',
				'role' => 'deleteAction metaAction',
				'data-parent' => '#'.$itemId,
			])
		?>
	</div>
	<?= '</li>' ?>
<?php else: ?>
	<?php
		if(!isset($widget)) {
			$widget = new \nitm\widgets\metadata\MetaInfo([
				'items' => [$model],
				'displayAs' => 'tags',
				'valuesOnly' => true,
				'attributes' => ['value'],
				'itemOptions' => function ($model) {
					return [
						'tag' => 'span',
						'after' => Html::tag('a', Icon::show('remove'), [
							'href' => '/tag/delete/'.$model->remote_type.'/'.$model->remote_id.'/'.$model->priority,
							'data-method' => 'post',
							'role' => 'deleteAction metaAction',
							'data-parent' => '#tag'.$model->getId(),
						])
					];
				}
			]);
		}
		echo $widget->renderTagItem($model, $model->priority);
	?>
<?php endif; ?>
