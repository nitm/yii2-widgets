'use strict';

// JavaScript Document

class Metadata extends BaseSortable {
	constructor() {
		super({
			id: 'metadata',
			buttons: {
				roles: {
					create: 'createMetadata',
					udpate: 'updateMetadata',
					remove: 'deleteMetadata',
					disable: 'disableParent'
				},
				create: 'createMetadata',
				udpate: 'updateMetadata',
				remove: 'deleteMetadata',
				disable: 'disableParent'
			},
			forms: {
				roles: {
					ajaxForm: 'metadataForm',
				}
			},
			inputs: {
				roles: {
					id: 'metadataId',
				}
			},
			views: {
				itemId: 'data',
				containerId: '[role~="metadata"]',
				roles: {
					item: 'metadataItem',
					template: 'metadataTemplate'
				}
			},
			defaultInit: [
				'initCreating',
				'initRemoving',
				'initForms'
			]
		});
	}

	initCreating(containerId, currentIndex) {
		var $container = $nitm.getObj((containerId == undefined) ? this.views.roles.containerId : containerId);
		$container.find("[role~='" + this.buttons.roles.create + "']")
			.map((i, elem) => {
				let $elem = $(elem);
				$elem.off('click');
				$elem.on('click', (e) => {
					e.preventDefault();
					let $localContainer = $($(e.currentTarget).data('target'));
					$container = $localContainer.length ? $localContainer : $container;
					var $template = $container.find("[role='" + this.views.roles.template + "']");
					var $clone = $template.clone(true);
					$clone.removeClass('hidden disabled')
						.attr('role', this.views.roles.item);
					var $deleteButton = $clone.find("[role~='" + this.buttons.roles.remove + "']");
					$deleteButton.attr('id', 'delete-metadata' + Date.now());
					$deleteButton.off('click');
					$deleteButton.on('click', function(e) {
						e.preventDefault();
						$nitm.module('tools').removeParent(e.currentTarget);
					});
					$clone.find("[role~='" + this.buttons.roles.disable + "']")
						.replaceWith($deleteButton);
					let cloneId = $clone.attr('id') + Date.now();
					$clone.attr('id', cloneId);
					$clone.find('form').data('parent', '#'+cloneId);
					$template.before($clone)
						.slideDown();
					this.initForms('#' + $clone.attr('id'));
					$nitm.m('tools')
						.initDefaults($clone.attr('id'));
				});
			});
	}

	afterAddToList(result, form, containerId) {
		this.afterCreate(result, form, containerId);
		this.initForms(containerId);
		this.initButtons(containerId);
		this.initRemoving(containerId);
	}

	afterUpdateListItem(result, form, containerId) {
		this.afterUpdate(result, form, containerId);
	}

	afterRemoveFromList(result, form, containerId) {
		this.afteDelete(result, form, containerId);
	}

	afterToggle(result, form, containerId) {
		this.afteUpdate(result, form, containerId);
	}

	initRemoving(containerId, currentIndex) {
		$nitm.module('tools').initDisableParent(containerId);
		$nitm.module('tools').initRemoveParent(containerId);
	}
}

$nitm.onModuleLoad('entity', function(module) {
	module.initModule(new Metadata());
}, null, 'metadata');
