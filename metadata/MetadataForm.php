<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace nitm\widgets\metadata;

use nitm\helpers\Html;
use nitm\helpers\WidgetHelper;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\builder\Form;
use kartik\builder\TabularForm;
use nitm\helpers\Icon;
use nitm\helpers\ArrayHelper;
use yii\bootstrap\ButtonGroup;

/**
 * ShortLink widget renders the address of a short link and a modal view button.
 */
class MetadataForm extends \yii\base\Widget
{

    /**
     * The header for the widget
     *
     * @var string
     */
    public $header;

    /**
     * The type of the widget
     *
     * @var string
     */
    public $type;

    /**
     * ActiveRecord model
     *
     * @var ActiveRecord
     */
    public $model;

    /**
     * Option for how the form appends data
     * 
     * One of [replace, append]
     *
     * @var string
     */
    public $appendMethod = 'append';

    /**
     * The options for the form
     *
     * @var array
     */
    public $formOptions = [
      'type' => 'horizontal',
    ];

    /**
     * Item options
     *
     * @var array
     */
    public $itemOptions = [];

    /**
     * Options for the action column
     *
     * @var array
     */
    public $actionColumnOptions = [];

    /**
     * SHould Pjax be anabled?
     *
     * @var boolean
     */
    public $enablePjax = false;

    /**
     * Enable the template? This is for adding multiple items dynamically
     *
     * @var boolean
     */
    public $enableTemplate = true;

    /**
     * DataProvider
     *
     * @var DataProvider
     */
    public $dataProvider;

    /**
     * The options for the grid
     *
     * @var array
     */
    public $gridOptions = [];

    /**
     * How should items be deletd?
     *
     * @var string
     */
    public $deleteMode = 'disable';

    /**
     * Callable or HTML string
     */
    public $contentAfter;

    /**
     * Callable or HTML string
     */
    public $contentBefore;

    /**
     * Extra options for the \kartik\builder\Form widget.
     *
     * @var array
     */
    public $formBuilder;

    /**
     * The controller route
     *
     * @var string
     */
    public $controllerRoute;

    /**
     * Options for the widget
     *
     * @var array
     */
    public $options = [];

    /**
     * Create button options
     *
     * @var array
     */
    public $createButton = [];

    /**
     * THe items for the widget
     *
     * @var array
     */
    public $items = [];

    /**
     * Display independent forms?
     *
     * @var boolean
     */
    public $independentForms = false;

    /**
     * Should this widget be displayed as a form?
     *
     * @var boolean
     */
    public $asForm = false;

    /**
     * Should the appender be added?
     *
     * @var boolean
     */
    public $withAppender = false;
    /*
     * 'attributes' => [
            'attribute', // attribute (in plain text)
            'description:html', // description attribute in HTML
            [
                'label' => 'Label',
                'value' => $value,
            ],
    * ]*/
    protected $_attributes = [];

    /**
     * The action url map
     * [
     *      {{action}} => {{url}}
     * ]
     * @var array
     */
    protected $actionUrls = [];

    /**
     * The uniqie ID for this widget
     *
     * @var string
     */
    protected $_uniqid;

    /**
     * ActiveForm form
     *
     * @var ActiveFOrm
     */
    protected $_form;

    public function init()
    {
        $this->_uniqid = uniqid();
        $this->options['id'] = 'metadata'.$this->_uniqid;
        $this->options['role'] = 'metadata';
        $allModels = $pagination = [];

        if (!$this->model instanceof \yii\db\ActiveRecord) {
            throw new \yii\base\InvalidConfigException('You need to provide a base model');
        }

        if (!isset($this->dataProvider) && !isset($this->queryDataProvider) && !isset($this->arrayDataProvider) && !$this->hasMethod('getDataProvider') && !is_array($this->dataProvider) && !($this->dataProvider instanceof \yii\data\BaseDataProvider)) {
            throw new \yii\base\InvalidConfigException("You need to set the dataProvider to either an array or a \yii\data\BaseDataProvider derived object");
        }

        if ($this->dataProvider instanceof \yii\data\BaseDataProvider) {
            $allModels = $this->dataProvider->getModels();
            $pagination = $this->dataProvider->pagination;
        }

        if ($this->enableTemplate) {
            $lastMetadataItem = clone $this->model;
            $lastMetadataItem->id = 'lastMetadataItem';
            $allModels['lastMetadataItem'] = $lastMetadataItem;
        }
        $this->dataProvider = new \yii\data\ArrayDataProvider([
           'allModels' => $allModels,
           'pagination' => $pagination,
        ]);

        assets\MetadataAsset::register($this->getView());
    }

    public function run()
    {
        $title = Html::tag('h2', $this->header);
        switch ($this->asForm) {
            case true:
            $metadata = $this->getAsForm();
            break;

            default:
            $metadata = $this->getAsGrid();
            break;
        }

        return $title.$metadata;
    }

    public function setAttributes($attributes)
    {
        $this->_attributes = $attributes;
    }

    public function getAttributes()
    {
        return $this->_attributes;
    }
    

    public function getAsForm()
    {
        $ret_val = '';
        $builderAttribtues = [];
        $models = array_filter($this->dataProvider->getModels());
        $models['lastMetadataItem'] = $this->model;
        foreach ($models as $idx => $item) {
            $item->setScenario($item->isNewRecord ? 'create': 'update');
            
            $attributes = $this->prepareAttributes($item, $idx);

            if ($this->independentForms) {
                $ret_val .= $this->getItemAsForm($attributes, $item, $idx, $item);
            } else {
                $builderAttribtues = $attributes;
            }
        }
        $this->attributes = $attributes;

        if ($this->independentForms) {
            $ret_val = Html::tag('div', $ret_val.$this->getAppender(), $this->options);
            if ($this->enablePjax) {
                ob_start();
                Pjax::begin([
                   'linkSelector' => '[role~="createMetadata"]',
                   'formSelector' => '[role~="metadataForm"]',
                ]);
                echo $ret_val;
                $ret_val = ob_get_contents();
                Pjax::end();
                ob_end_clean();
            }

            return $ret_val;
        } else {
            $ret_val = TabularForm::widget(array_merge([
                'form' => $this->form,
                'attributes' => $builderAttribtues,
                'actionColumn' => $this->getActions(),
                'dataProvider' => $this->dataProvider,
                'gridSettings' => [
                    'pjax' => $this->enablePjax,
                    'rowOptions' => function ($item, $idx) {
                        return [
                            'class' => 'kv-tabform-row '.(($idx === 'lastMetadataItem') ? 'hidden' : 'visible bg-success'),
                            'id' => ($idx === 'lastMetadataItem') ? 'metadata-template'.$this->_uniqid : 'metadata-item'.$item->getId(),
                            'role' => ($idx === 'lastMetadataItem') ? 'metadataTemplate' : 'metadataItem',
                        ];
                    },
                ],
            ], $this->gridOptions));

            return Html::tag('div', $ret_val.$this->getAppender(), $this->options);
        }
    }

    public function prepareAttributes($item, $idx=1) 
    {
        $allAttributes = $this->attributes;
        $attributeCount = count($allAttributes);
        $type = $this->type;

        if (ArrayHelper::getValue($this->formOptions, 'type', 'inline') === 'inline') {
            $columnCount = ($attributeCount + $invisible);
            $columnSize = floor(12 / $columnCount >= 1 ? $columnCount : 1);
        } else {
            $columnSize = 12;
        }
        $invisible = 0;
        array_walk($allAttributes, function ($attr) use (&$invisible) {
            if (ArrayHelper::getValue($attr, 'columnOptions.visible', true) === false) {
                --$invisible;
            }
        });

        $attributes = [];
        array_walk($allAttributes, function ($value, $attribute) use (&$attributes, $columnSize, $type, $item, $idx) {
            $attribute = is_numeric($attribute) ? ArrayHelper::getValue($value, 'label', !$attribute || is_numeric($attribute) ? $value : $attribute) : $attribute;
            if(is_array($value)) {
                if(isset($value['value']) && is_callable($value['value'])) {
                    $value['value'] = call_user_func_array($value['value'], [
                        $item, $idx
                    ]);
                } else {
                    $value['value'] = ArrayHelper::getValue($value, 'value', ArrayHelper::getValue($item, $attribute));
                }
            } else {
                $value = ['value' => ArrayHelper::getValue($item, $attribute)];
            }
            $value['options'] = array_merge([
                'name' => !empty($type) ? $item->formName()."[$type][$attribute]" : $item->formName()."[$attribute]"
            ], ArrayHelper::getValue($value, 'options', []));
            $value['label'] = ArrayHelper::getValue($value, 'label', Html::activeLabel($this->model, ArrayHelper::getValue($attribute, 'label', $attribute)));
            if (!$this->independentForms) {
                $value['columnOptions'] = ArrayHelper::getValue($value, 'columnOptions', [
                    'options' => [
                        'class' => "col-md-$columnSize col-lg-$columnSize col-sm-12",
                    ],
                ]);

                $value = array_merge([
                    'options' => ['class' => 'form-control form-'.ArrayHelper::getValue($this->allFormOptions, 'type', 'inline')],
                    'label' => null,
                ], $value);

                $value['options']['name'] .= '[]';
            }
            if ($idx === 'lastMetadataItem' && !ArrayHelper::remove($value, 'keep', false)) {
                $value['options']['value'] = '';
            } else {
                $value['options']['value'] = ArrayHelper::remove($value, 'value', ArrayHelper::getValue($item, $attribute));
            }
            //unset($value['value']);
            $value['type'] = ArrayHelper::getValue($value, 'type', Form::INPUT_TEXT);
            $attributes[strtolower($attribute)] = $value;
        });
        return $attributes;
    }

    public function getItemAsForm($attributes, $item, $idx, $model = null)
    {
        $itemAttributes = $this->setActions($item, $attributes, $idx);
        $options = array_merge([
            'id' => ($idx === 'lastMetadataItem') ? 'metadata-template'.$this->_uniqid : 'metadata-item'.$item->getId(),
            'role' => ($idx === 'lastMetadataItem') ? 'metadataTemplate' : 'metadataItem',
            'tag' => 'div',
            'style' => 'padding-top: 15px; margin-bottom: 20px',
            'class' => 'col-sm-12',
        ], $this->itemOptions);

        Html::addCssClass($options, ($idx === 'lastMetadataItem') ? 'hidden' : 'visible bg-success');
        $ret_val = $this->independentForms ? Html::beginTag('div', $options) : '';
        $attributeCount = count($attributes);
        if (!isset($attributes['id'])) {
            $idName = !empty($type) ? $item->formName()."[$type][id]" : $item->formName().'[id]';
            if (!$this->independentForms) {
                $idName .= '[]';
            }
            $attributes['id'] = [
              'type' => FORM::INPUT_RAW,
              'hidden' => true,
              'value' => Html::activeHiddenInput($item, 'id', [
                  'name' => $idName,
                  'role' => 'metadataId',
              ]),
          ];
        }
        ob_start();
        $model = $item ?: $this->model;
        $contentBefore = $this->contentBefore;
        if(is_callable($contentBefore)) {
            $contentBefore = $contentBefore($model);
        }
        if(is_string($contentBefore)) {
            echo $contentBefore;
        }
        $widget = Form::begin(array_merge((array) $this->formBuilder, [
            'model' => $item ?: $this->model,
            'form' => $this->getForm([], $item ?: $this->model),
            'autoGenerateColumns' => false,
            'attributes' => $attributes,
            'options' => ['tag' => false],
        ]));
        $widget->end();
        $widget->form->end();
        $contentAfter = $this->contentAfter;
        if(is_callable($contentAfter)) {
            $contentAfter = $contentAfter($model);
        }
        if(is_string($contentAfter)) {
            echo $contentAfter;
        }
        $ret_val .= ob_get_clean();
        $ret_val .= $this->independentForms ? Html::endTag('div') : '';

        return $ret_val;
    }

    public function setForm($form)
    {
        $this->_form = $form;
    }

    public function getAsGrid()
    {
        return GridView::widget([
            'dataProvider' => $this->dataProvider,
            'options' => [
                'role' => 'metadata',
                'id' => 'metadata'.$this->_uniqid,
            ],
            'showHeader' => false,
            'showFooter' => false,
            'export' => false,
            'summary' => false,
            'layout' => '{items}',
            'columns' => array_merge($this->attributes, [
                $this->getActions(),
            ]),
        ]);
    }

    protected function getAllFormOptions()
    {
        return array_merge(ArrayHelper::getValue($this->form, 'options', []), (array) $this->formOptions);
    }

    private function getAppender()
    {
        if ($this->withAppender) {
            $title = Icon::forAction('create').' '.ArrayHelper::remove($this->createButton, 'text', Html::tag('span', 'Add '.$this->model->properName(), [
             'class' => 'visible-md-inline',
           ]));
            $ret_val = Html::beginTag('div', [
                'class' => 'text-right',
            ]);
            $ret_val .= Html::button($title, array_merge([
                    'title' => \Yii::t('yii', 'Add '.$this->model->properName()),
                    'class' => 'btn btn-primary right-floated',
                    'data-pjax' => '0',
                    'data-url' => $this->createUrl($this->model),
                    'style' => 'width: '.($this->formOptions['type'] == 'horizontal' ? '100%' : 'auto').';margin-top: 1rem'
                ], $this->createButton, [
                    'role' => 'createMetadata',
                    'data-target' => '#'.$this->options['id'],
                ]));
            $ret_val .= Html::endTag('div');

            return $ret_val;
        }

        return '';
    }

    protected function getForm($options = [], $model = null)
    {
        if (!isset($this->_form) || $this->independentForms) {
            $options = array_merge($this->formOptions, $options);
            $action = ArrayHelper::getValue($options, 'action');
            if ($action && is_callable($action)) {
                $options['action'] = $action($model);
            }
            $class = ArrayHelper::remove($options, 'class', \kartik\widgets\ActiveForm::className());
            $this->_form = $class::begin(array_merge([
                'enableAjaxValidation' => true,
                'options' => [
                    'role' => 'metadataForm',
                    'id' => 'metadata-form'.$this->_uniqid,
                    'data-parent' => '#'.$this->options['id'],
                    'data-append-content-method' => $this->appendMethod
                ],
            ], (array) $options));
        }

        return $this->_form;
    }

    private function setActions($item, &$attributes, $idx)
    {
        $saveAction = $item->isNewRecord ? 'create' : 'update';
        $saveRole = $item->isNewRecord ? 'saveMetadata' : 'udpateMetadata';
        $deleteAction = (string)$idx === 'lastMetadataItem' ? 'delete' : ($this->deleteMode == 'disable' ? 'disable' : 'delete');
        $deleteRole = (string)$idx === 'lastMetadataItem' ? 'deleteMetadata' : ($this->deleteMode == 'disable' ? 'disableParent' : 'deleteMetadata');
        $attributes['actions'] = array_replace_recursive([
            'type' => Form::INPUT_RAW,
            'label' => 'Delete',
            'columnOptions' => [
                'colspan' => 1,
            ],
            'value' => WidgetHelper::widget(ButtonGroup::class, [
                'encodeLabels' => false,
                'options' => [
                    'class' => 'pull-right'
                ],
                'buttons' => [
                    [
                        'label' => Html::tag('span', 'Save '.$this->model->properName(), [
                            'class' => 'visible-md-inline',
                        ]),
                        'options' => [
                            'labeled' => true,
                            'icon' => 'save',
                            'title' => \Yii::t('yii', 'Save '.$this->model->properName()),
                            'data-pjax' => 0,
                            'role' => $saveRole,
                            'class' => 'btn btn-'.($item->isNewRecord ? 'success' : 'info'),
                            'type' => $this->independentForms ? 'submit' : 'button',
                        ],
                    ], [
                        'label' => Html::tag('span', 'Delete '.$this->model->properName(), [
                            'class' => 'visible-md-inline',
                        ]),
                        'options' => [
                            'labeled' => true,
                            'icon' => $deleteAction,
                            'title' => \Yii::t('yii', 'Delete '.$this->model->properName()),
                            'data-pjax' => 0,
                            'role' => $deleteRole,
                            'inline' => true,
                            'data-action' => $this->createUrl($item, 'delete'),
                            'data-parent' => '[role~="metadataItem"]',
                            'data-depth' => 1,
                            'class' => 'btn btn-danger',
                        ],
                    ],
                ],
            ]),
        ], $this->actionColumnOptions);
    }

    private function getActions()
    {
        return array_replace_recursive([
            'class' => 'nitm\grid\ActionColumn',
            'buttons' => [
                'form/update-metadata' => function ($url, $model) {
                    return \nitm\widgets\modal\Modal::widget([
                        'toggleButton' => [
                            'tag' => 'a',
                            'label' => Icon::forAction('pencil'),
                            'href' => $this->createUrl([$url, '_format' => 'modal']),
                            'title' => \Yii::t('yii', 'Edit '.$model->properName()),
                            'role' => 'dynamicAction updateAction disabledOnClose updateMetadata',
                        ],
                    ]);
                },
                'delete-metadata' => function ($url, $model) {
                    return Html::a(Icon::forAction('delete'),  $url, [
                        'title' => \Yii::t('yii', 'Delete '.$model->properName()),
                        'role' => 'metaAction deleteAction deleteMetadata',
                        'data-depth' => 1,
                        'data-parent' => '[role~="metadataItem"]',
                        'data-pjax' => '0',
                        'data-force' => 1,
                    ]);
                },
            ],
            'template' => '{form/update-metadata} {delete-metadata}',
            'urlCreator' => function ($action, $model, $key, $index) {
                return $this->createUrl($model, $action);
            },
            'options' => [
                'rowspan' => 2,
                'class' => 'col-md-1 col-lg-1',
            ],
        ], $this->actionColumnOptions);
    }

    /**
     * Help create proper urls.
     *
     * @param array|model $model  The model or array of options
     * @param string      $action The action being performed
     * @param array       $params Additional parameters for the array
     *
     * @return string The URL
     */
    protected function createUrl($model, $action = null, $params = [])
    {
        if (is_array($model)) {
            return \Yii::$app->urlManager->createUrl($model);
        } else {
            $controllerRoute = !$this->controllerRoute ? $model->isWhat() : $this->controllerRoute;
            if ($model->id) {
                $routePath = is_null($action) ? '/'.$model->getId() : '/'.$action.'/'.$model->getId();
            } else {
                $routePath = '/'.$action;
            }

            return \Yii::$app->urlManager->createUrl(array_merge([$controllerRoute.$routePath], $params));
        }
    }
}
