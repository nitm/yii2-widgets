<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, NitmGeography.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\widgets\metadata;

use nitm\helpers\Html;
use nitm\helpers\Icon;
use kartik\widgets\SwitchInput;
use kartik\time\TimePicker;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@ninjasitm.com>
 *
 * @since 1.0
 */
class MetadataListForm extends MetadataForm
{
    public $primaryModel;
    public $isVisible = false;
    public $enableHeaders = true;
    public $title;
    public $route;
    public $appendMethod = 'replace';
    public $withForm = true;
    public $withAppender = true;
    public $asForm = true;
    public $independentForms = true;
    public $modelClass;
    public $listView;
    public $dataProviderAttribute;

    public function init()
    {
        $this->model = $this->model ?? $this->createListModel();
        $this->options['id'] = $this->model->is.'-container';
        $this->options['style'] = $this->isVisible ? 'display: block' : 'display: none';
        $this->formOptions['action'] = function ($model) {
            $action = !$model->isNewRecord ? 'update-list-item' : 'add-to-list';

            return \Yii::$app->urlManager->createUrl([$this->route.'/'.$action, 'type' => $this->model->is, 'id' => $model->id]);
        };
        parent::init();
        $this->title = $this->getTitle();
    }

    public function setHeader()
    {
        if($this->enableHeaders) {
            $icon = Icon::show($this->isVisible ? 'chevron-down' : 'chevron-right');
            $this->header = Html::tag('h2', Html::a($icon.' '.Html::encode($this->title), '#', [
                'role' => 'visibility',
                'data-id' => '#'.$this->options['id'],
            ]), [
                'class' => 'clickable',
            ]);
        }
    }

    public function createListModel($options = [])
    {
        $modelClass = $this->modelClass;

        return new $modelClass($options);
    }

    public function run()
    {
        $this->setHeader();
        $this->dataProvider = $this->getDataProvider();

        return parent::run();
    }

    public function getDataProvider()
    {
        $modelClass = $this->modelClass;
        $dataProvider = $this->dataProviderAttribute;
        $items = $this->primaryModel->$dataProvider();

        return new \yii\data\ArrayDataProvider([
           'allModels' => $items ? $items : [],
        ]);
    }

    protected function getSwitchInput($options = [])
    {
        return array_merge([
         'type' => 'widget',
         'widgetClass' => SwitchInput::class,
         'options' => [
            'id' => 'switch-'.uniqid(),
            'pluginOptions' => [
               'onText' => 'Yes',
               'offText' => 'No',
            ],
         ],
      ], $options);
    }

    protected function getTimePickerInput($options = [])
    {
        return array_merge([
         'type' => 'widget',
         'widgetClass' => TimePicker::class,
         'options' => [
            'id' => 'time-'.uniqid(),
         ],
         'pluginOptions' => [
            'showMeridian' => false,
            'showInputs' => false,
         ],
      ], $options);
    }

    public function getTitle() {
        $action = $this->isVisible ? 'Hide' : 'Show';
        return isset($this->title) ? $this->title : $action." ".$this->model->properName($this->model->isWhat(true));
    }
}
