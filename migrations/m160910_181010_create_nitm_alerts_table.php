<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_alerts`.
 */
class m160910_181010_create_nitm_alerts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_alerts');
        if ($tableSchema) {
            return true;
        }

        $this->createTable('nitm_alerts', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer(),
            'remote_type' => $this->string(64),
            'remote_for' => $this->string(64),
            'remote_id' => $this->integer(),
            'user_id' => $this->integer(),
            'action' => $this->string(32),
            'message' => $this->string(255),
            'actor' => $this->text(),
            'object' => $this->text(),
            'global' => $this->boolean()->defaultValue(false),
            'disabled' => $this->boolean()->defaultValue(false),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('nitm_alerts');
    }
}
