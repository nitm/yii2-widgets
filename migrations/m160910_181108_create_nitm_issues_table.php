<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_issues`.
 */
class m160910_181108_create_nitm_issues_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_issues');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_issues', [
            'id' => $this->primaryKey(),
            'status' => $this->integer(),
            'author_id' => $this->integer(),
            'parent_type' => $this->string(64),
            'parent_id' => $this->integer(),
            'notes' => $this->text()->notNull(),
            'resolved' => $this->boolean()->defaultValue(false),
            'resolved_by' => $this->integer(),
            'resolved_at' => $this->timestamp(),
            'closed' => $this->boolean()->defaultValue(false),
            'closed_by' => $this->integer(),
            'closed_at' => $this->timestamp(),
            'duplicate' => $this->boolean()->defaultValue(false),
            'duplicated_at' => $this->timestamp(),
            'duplicated_by' => $this->integer(),
            'duplicate_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('nitm_issues');
    }
}
