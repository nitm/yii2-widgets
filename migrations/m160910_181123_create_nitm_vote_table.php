<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_vote`.
 */
class m160910_181123_create_nitm_vote_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_votes');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_votes', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer(),
            'parent_type' => $this->string(64),
            'parent_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('nitm_votes');
    }
}
