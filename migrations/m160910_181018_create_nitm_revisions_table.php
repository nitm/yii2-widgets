<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_revisions`.
 */
class m160910_181018_create_nitm_revisions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_revisions');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_revisions', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer(),
            'parent_type' => $this->string(64),
            'parent_id' => $this->integer(),
            'data' => $this->text()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('nitm_revisions');
    }
}
