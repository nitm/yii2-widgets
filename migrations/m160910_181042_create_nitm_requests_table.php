<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_requests`.
 */
class m160910_181042_create_nitm_requests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_requests');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_requests', [
            'id' => $this->primaryKey(),
            'title' => $this->string(64),
            'status' => $this->string(16),
            'author_id' => $this->integer(),
            'request_for_id' => $this->integer(),
            'type_id' => $this->integer(),
            'status_id' => $this->integer(),
            'request' => $this->text()->notNull(),
            'editor_id' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'edits' => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'closed' => $this->boolean()->defaultValue(false),
            'closed_by' => $this->integer(),
            'closed_at' => $this->timestamp(),
            'completed' => $this->boolean()->defaultValue(false),
            'completed_by' => $this->integer(),
            'completed_at' => $this->timestamp(),
            'rating' => $this->boolean()->defaultValue(false),
            'rated_on' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('nitm_requests');
    }
}
