<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_replies`.
 */
class m160910_181054_create_nitm_replies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_replies');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_replies', [
            'id' => $this->primaryKey(),
            'title' => $this->string(64),
            'priority' => $this->string(16),
            'author_id' => $this->integer(),
            'parent_type' => $this->string(64),
            'parent_id' => $this->integer(),
            'message' => $this->text()->notNull(),
            'cookie_hash' => $this->text()->notNull(),
            'ip_addr' => $this->text()->notNull(),
            'ip_host' => $this->text()->notNull(),
            'reply_to' => $this->integer()->null(),
            'reply_to_author_id' => $this->integer()->null(),
            'editor_id' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'hidden' => $this->boolean()->defaultValue(false),
            'deleted' => $this->boolean()->defaultValue(false),
            'public' => $this->boolean()->defaultValue(false),
            'disabled' => $this->boolean()->defaultValue(false),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('nitm_replies');
    }
}
